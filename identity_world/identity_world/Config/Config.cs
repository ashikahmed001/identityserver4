﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace identity_world.Config
{
    public static class Config
    {

        public static IEnumerable<Client> GetClient() {

            return new List<Client> {
                new Client{
                    ClientId="Client1",
                    ClientName="Client No. 1",
                    AllowedGrantTypes=GrantTypes.ClientCredentials,

                    ClientSecrets={ new Secret("Clientx1".Sha256()) },
                    AllowedScopes={"World" }
                },

                new Client{
                    ClientId="Client2",
                    ClientName="Client No. 2",
                    AllowedGrantTypes=GrantTypes.ResourceOwnerPassword,

                    ClientSecrets={ new Secret("Clientx2".Sha256())},
                    AllowedScopes={ "New", "World"}
                }
            };
            
        }

        public static IEnumerable<ApiResource> GetApiResources() {
            return new List<ApiResource>
            {
                new ApiResource("New", "New API"),
                new ApiResource("World", "World API"),
                new ApiResource("Order", "Order API")
            };
        }

        public static List<TestUser> GetTestUsers() {
            return new List<TestUser> {
                new TestUser
                {
                    Username="John",
                    Password="test123",
                    SubjectId="1"
                },
                new TestUser
                {
                    Username="Jack",
                    Password="abc123",
                    SubjectId="2"
                }
            };
        }

    }
}
