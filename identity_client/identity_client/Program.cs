﻿using IdentityModel.Client;
using System;
using System.Threading.Tasks;

namespace identity_client
{
    enum RequestType {
        RequestClientCredentials,
        RequestResourceOwnerPassword
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            GetToken(RequestType.RequestClientCredentials).GetAwaiter().GetResult();
            Console.ReadLine();
            Console.ReadKey(false);
        }

        static async Task GetToken(RequestType type) {
            var discovery = await DiscoveryClient.GetAsync("http://localhost:64347/");
            TokenResponse response = null;
            TokenClient tokenClient = null;
            if (discovery.IsError)
            {
                Console.WriteLine("Discovery Error: "+discovery.Error);
                return ;
            }

            Console.WriteLine("Discovery JSON: "+discovery.Json);

            switch (type)
            {
                case RequestType.RequestClientCredentials:
                    tokenClient = new TokenClient(discovery.TokenEndpoint, "Client1", "Clientx1");
                    response = await tokenClient.RequestClientCredentialsAsync("World");
                    break;
                case RequestType.RequestResourceOwnerPassword:
                    tokenClient = new TokenClient(discovery.TokenEndpoint, "Client2", "Clientx2");
                    response = await tokenClient.RequestResourceOwnerPasswordAsync("Jack", "abc123");
                    
                    break;
            }
                
            if (response.IsError)
            {
                Console.WriteLine("Response Error: " + response.Error);
                return;
            }

            Console.WriteLine("Response JSON: "+response.Json);
        }

        
    }
}
